package me.jadethecat.bread.world.dimension;

import me.jadethecat.bread.block.BreadBlocks;
import me.jadethecat.bread.world.BreadWorldGen;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;


public class TeleporterBread extends Teleporter
{
    private final BlockPos position;
    public TeleporterBread(WorldServer worldIn, BlockPos pos) {
        super(worldIn);
        position = pos;
    }

    @Override
    public void placeInPortal(Entity entityIn, float rotationYaw) {
        if (!this.placeInExistingPortal(entityIn, rotationYaw)) {
            this.makePortal(entityIn);
            this.placeInExistingPortal(entityIn, rotationYaw);
        }
    }

    @Override
    public boolean placeInExistingPortal(Entity entityIn, float rotationYaw) {
        if (this.world.getBlockState(position) != BreadBlocks.BREAD_DIM_TELEPORTER.getDefaultState()) {
            return false;
        } else {
            this.world.setBlockState(position.up(), Blocks.AIR.getDefaultState());
            this.world.setBlockState(position.up(2), Blocks.AIR.getDefaultState());
            entityIn.setLocationAndAngles((double)position.getX() + 0.5, (double)position.up().getY(), (double)position.getZ() + 0.5, rotationYaw, 0.0F);
            entityIn.motionX = 0.0D;
            entityIn.motionY = 0.0D;
            entityIn.motionZ = 0.0D;
            return true;
        }
    }

    @Override
    public boolean makePortal(Entity entityIn) {
        this.world.setBlockState(position, BreadBlocks.BREAD_DIM_TELEPORTER.getDefaultState());
        this.world.setBlockState(position.up(), Blocks.AIR.getDefaultState());
        this.world.setBlockState(position.up(2), Blocks.AIR.getDefaultState());
        return true;
    }


    /* Copypasta from Vanilla */
    public class PortalPosition extends BlockPos
    {
        /** The worldtime at which this PortalPosition was last verified */
        public long lastUpdateTime;

        public PortalPosition(BlockPos pos, long lastUpdate)
        {
            super(pos.getX(), pos.getY(), pos.getZ());
            this.lastUpdateTime = lastUpdate;
        }
    }
}
