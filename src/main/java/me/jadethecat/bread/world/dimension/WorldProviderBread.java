package me.jadethecat.bread.world.dimension;

import me.jadethecat.bread.world.BreadWorldGen;
import net.minecraft.init.Biomes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.BiomeProvider;
import net.minecraft.world.biome.BiomeProviderSingle;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.IChunkGenerator;

import javax.annotation.Nullable;

public class WorldProviderBread extends WorldProvider {
    @Override
    public DimensionType getDimensionType() {
        return BreadWorldGen.BREAD_DIMENSION;
    }

    @Override
    public BiomeProvider getBiomeProvider() {
        return new BiomeProviderSingle(Biomes.FROZEN_OCEAN);
    }



    @Nullable
    @Override
    public String getSaveFolder() {
        return "BREAD";
    }

    @Override
    public boolean canRespawnHere() {
        return false;
    }

    @Override
    public IChunkGenerator createChunkGenerator() {
        return new ChunkGeneratorBread(world);
    }

    @Override
    public boolean isSurfaceWorld() {
        return true;
    }

    @Override
    public boolean canDoRainSnowIce(Chunk chunk) {
        return false;
    }

    @Override
    public boolean canSnowAt(BlockPos pos, boolean checkLight) {
        return false;
    }
}
