package me.jadethecat.bread.world;

import me.jadethecat.bread.world.dimension.WorldProviderBread;
import net.minecraft.world.DimensionType;
import net.minecraftforge.common.DimensionManager;

import java.awt.*;

public class BreadWorldGen {
    public static final int BREAD_ID = 2;
    public static final DimensionType BREAD_DIMENSION = DimensionType.register("Bread", "_bread", BREAD_ID, WorldProviderBread.class, true);

    public static final void registerDimensions() {
        DimensionManager.registerDimension(BREAD_ID, BREAD_DIMENSION);

    }

    private static Integer getDimID() {
        for (int i = 2; i < Integer.MAX_VALUE; i++) {
            if (!DimensionManager.isDimensionRegistered(i)) {
                return i;
            }
        }
        return null;
    }
}
