package me.jadethecat.bread;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class CommandTPtoBread implements ICommand {
    @Override
    public String getName() {
        return "tpbread";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "tpbread";
    }

    @Override
    public List<String> getAliases() {
        List<String> a = new ArrayList<>();
        a.add("tpbread");
        return a;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayer) {
            EntityPlayer sender2 = (EntityPlayer) sender;
            sender2.changeDimension(2, (world, entity, yaw) -> {

            });
        }
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    @Override
    public int compareTo(ICommand o) {
        return 0;
    }
}
