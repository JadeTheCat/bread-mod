package me.jadethecat.bread.proxy;

import me.jadethecat.bread.lib.Constants;
import me.jadethecat.bread.world.BreadWorldGen;
import net.minecraft.block.BlockLeaves;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod.EventBusSubscriber(modid = Constants.MODID)
public class CommonProxy {
    public void preInit(FMLPreInitializationEvent e) {
        BreadWorldGen.registerDimensions();
    }
    public void init(FMLInitializationEvent e) {
    }
    public void postInit(FMLPostInitializationEvent e) {

    }
    public void setFancyGraphics(BlockLeaves blockIn) {

    }
}
