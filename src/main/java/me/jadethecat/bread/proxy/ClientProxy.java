package me.jadethecat.bread.proxy;

import me.jadethecat.bread.block.BreadBlocks;
import me.jadethecat.bread.client.BreadBlockColors;
import me.jadethecat.bread.client.BreadItemColors;
import me.jadethecat.bread.item.BreadItems;
import me.jadethecat.bread.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLeaves;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = Constants.MODID)
public class ClientProxy extends CommonProxy {

    @Override
    public void init(FMLInitializationEvent e) {
        super.init(e);
        BreadBlockColors.registerBlockColors();
        BreadItemColors.registerItemColors();
    }

    @SubscribeEvent
    public static void registerRenders(ModelRegistryEvent e) {
        registerItemRenderer(BreadItems.BREAD_SLICE, 0, "bread_slice");
        registerItemRenderer(BreadItems.KNIFE, 0, "knife");
        registerItemRenderer(BreadItems.SLICED_HAM, 0 , "sliced_ham");
        registerItemRenderer(BreadItems.SALT, 0, "salt");
        registerItemRenderer(BreadItems.SALT_BUCKET, 0, "salt_bucket");
        registerItemRenderer(BreadItems.CHEDDAR, 0, "cheddar");
        registerItemRenderer(BreadItems.BUTTER, 0, "butter");

        registerItemRenderer(BreadItems.SANDWICH_HAM, 0, "sandwich");
        registerItemRenderer(BreadItems.SANDWICH_HAM_CHEDDAR, 0, "sandwich");

        registerBlockModel(BreadBlocks.BREADWOOD_LOG, 0, "breadwood_log");
        registerBlockModel(BreadBlocks.BREADWOOD_LEAVES, 0, "breadwood_leaves");
        registerBlockModel(BreadBlocks.BREADWOOD_SAPLING, 0, "breadwood_sapling");
        registerBlockModel(BreadBlocks.BREADWOOD_PLANKS, 0, "breadwood_planks");
        registerBlockModel(BreadBlocks.BREADWOOD_PLANK_STAIRS, 0, "breadwood_plank_stairs");
        registerBlockModel(BreadBlocks.BREAD_DIM_TELEPORTER, 0, "dimension_teleporter");

        registerItemRenderer(BreadBlocks.ITEM_BLOCK_BREADWOOD_LOG, 0, "breadwood_log");
        registerItemRenderer(BreadBlocks.ITEM_BLOCK_BREADWOOD_LEAVES, 0, "breadwood_leaves");
        registerItemRenderer(BreadBlocks.ITEM_BLOCK_BREADWOOD_SAPLING, 0, "breadwood_sapling");
        registerItemRenderer(BreadBlocks.ITEM_BLOCK_BREADWOOD_PLANKS, 0, "breadwood_planks");
        registerItemRenderer(BreadBlocks.ITEM_BLOCK_BREADWOOD_PLANK_STAIRS, 0, "breadwood_plank_stairs");
        registerItemRenderer(BreadBlocks.ITEM_BLOCK_BREAD_DIM_TELEPORTER, 0, "dimension_teleporter");
    }

    public static void registerItemRenderer(Item item, int meta, String id) {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(Constants.MODID + ":" + id, "inventory"));
    }
    public static void registerBlockModel(Block parBlock, int parMetaData, String id) {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(parBlock), parMetaData,
                new ModelResourceLocation(Constants.MODID + ":" + id, "inventory"));
    }

    @Override
    public void setFancyGraphics(BlockLeaves blockIn) {
        blockIn.setGraphicsLevel(true);
    }
}
