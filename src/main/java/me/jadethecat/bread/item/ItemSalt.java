package me.jadethecat.bread.item;

import me.jadethecat.bread.BreadMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class ItemSalt extends ItemFood {
    public ItemSalt() {
        super(0, 0, false);
        setUnlocalizedName("salt");
        setRegistryName("salt");
        setCreativeTab(BreadMod.TAB_BREAD_MOD);
        setAlwaysEdible();
    }

    @Override
    protected void onFoodEaten(ItemStack stack, World worldIn, EntityPlayer player) {
        if (!worldIn.isRemote) {
            player.addPotionEffect(new PotionEffect(MobEffects.HUNGER, 60*5, 2, false, true));
        }
    }

}
