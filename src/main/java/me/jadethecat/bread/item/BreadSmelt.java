package me.jadethecat.bread.item;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BreadSmelt {
    public static void init() {
        GameRegistry.addSmelting(Items.WATER_BUCKET, new ItemStack(BreadItems.SALT_BUCKET), 5.0f);
    }
}
