package me.jadethecat.bread.item;

import me.jadethecat.bread.BreadMod;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;

public class ItemSandwich extends ItemFood {
    private int healAmt = 2;
    private float saturation = 1.0f;

    public ItemSandwich(ItemFood... foods) {
        super(1, 1.0f, false);
        StringBuilder builder = new StringBuilder();
        builder.append("sandwich_");
        healAmt += ((ItemFood)BreadItems.BREAD_SLICE).getHealAmount(new ItemStack(BreadItems.BREAD_SLICE)) * 2;
        saturation += ((ItemFood)BreadItems.BREAD_SLICE).getSaturationModifier(new ItemStack(BreadItems.BREAD_SLICE)) * 2;
        for (int i = 0; i < foods.length; i++) {
            healAmt += foods[i].getHealAmount(new ItemStack(foods[i]));
            saturation += foods[i].getSaturationModifier(new ItemStack(foods[i]));
            builder.append(foods[i].getUnlocalizedName().replace("item.", ""));
            if (i != foods.length - 1) {
                builder.append("_");
            }
        }
        setUnlocalizedName(builder.toString());
        setRegistryName(builder.toString());
        setCreativeTab(BreadMod.TAB_BREAD_MOD);
    }

    @Override
    public int getHealAmount(ItemStack stack) {
        return healAmt;
    }

    @Override
    public float getSaturationModifier(ItemStack stack) {
        return saturation;
    }
}
