package me.jadethecat.bread.item;

import me.jadethecat.bread.BreadMod;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class ItemSaltBucket extends Item {
    public ItemSaltBucket() {
        setContainerItem(Items.BUCKET);
        setUnlocalizedName("salt_bucket");
        setRegistryName("salt_bucket");
        setCreativeTab(BreadMod.TAB_BREAD_MOD);
    }
}
