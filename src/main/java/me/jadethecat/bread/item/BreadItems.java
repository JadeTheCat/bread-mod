package me.jadethecat.bread.item;

import me.jadethecat.bread.BreadMod;
import me.jadethecat.bread.lib.Constants;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = Constants.MODID)
public class BreadItems {
    public static Item BREAD_SLICE;
    public static Item KNIFE;
    public static Item SLICED_HAM;
    public static Item SALT;
    public static Item SALT_BUCKET;
    public static Item CHEDDAR;
    public static Item BUTTER;
    public static Item TOAST;
    public static Item BURNT_BREAD;

    public static Item SANDWICH_HAM;
    public static Item SANDWICH_HAM_CHEDDAR;

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> e) {
        BREAD_SLICE = new ItemFood(1, 0.5f, false).setUnlocalizedName("bread_slice").setRegistryName("bread_slice").setCreativeTab(BreadMod.TAB_BREAD_MOD);
        KNIFE = new ItemKnife();
        SLICED_HAM = new ItemFood(1, 0.0f, false).setUnlocalizedName("sliced_ham").setRegistryName("sliced_ham").setCreativeTab(BreadMod.TAB_BREAD_MOD);
        SALT = new ItemSalt();
        SALT_BUCKET = new ItemSaltBucket();
        CHEDDAR = new ItemFood(1, 0.5f, false).setUnlocalizedName("cheddar").setRegistryName("cheddar").setCreativeTab(BreadMod.TAB_BREAD_MOD);
        BUTTER = new ItemFood(1, 0.5f, false).setUnlocalizedName("butter").setRegistryName("butter").setCreativeTab(BreadMod.TAB_BREAD_MOD);

        SANDWICH_HAM = new ItemSandwich((ItemFood) SLICED_HAM);
        SANDWICH_HAM_CHEDDAR = new ItemSandwich((ItemFood) SLICED_HAM, (ItemFood) CHEDDAR);

        e.getRegistry().registerAll(BREAD_SLICE, KNIFE, SLICED_HAM, SALT, SALT_BUCKET, SANDWICH_HAM, CHEDDAR, BUTTER, SANDWICH_HAM_CHEDDAR);
    }
}
