package me.jadethecat.bread.client;

import me.jadethecat.bread.block.BreadBlocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.ColorizerFoliage;

public class BreadItemColors implements IItemColor {

    public static final IItemColor INSTANCE = new BreadItemColors();

    @Override
    public int colorMultiplier(ItemStack stack, int tintIndex) {
        return ColorizerFoliage.getFoliageColorBasic();
    }
    public static void registerItemColors() {
        Minecraft.getMinecraft().getItemColors().registerItemColorHandler(INSTANCE, BreadBlocks.BREADWOOD_LEAVES);
    }
}
