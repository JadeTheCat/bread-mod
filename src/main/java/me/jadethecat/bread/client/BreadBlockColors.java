package me.jadethecat.bread.client;

import me.jadethecat.bread.block.BreadBlocks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.IBlockColor;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ColorizerFoliage;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.biome.*;
import net.minecraftforge.common.BiomeManager;

import javax.annotation.Nullable;
import java.util.Random;

public class BreadBlockColors implements IBlockColor {

    public static final IBlockColor INSTANCE = new BreadBlockColors();
    public static Random rand = new Random();

    @Override
    public int colorMultiplier(IBlockState state, @Nullable IBlockAccess worldIn, @Nullable BlockPos pos, int tintIndex) {
        return worldIn != null && pos != null ? BiomeColorHelper.getFoliageColorAtPos(worldIn, pos) : ColorizerFoliage.getFoliageColorBasic();
    }
    public static void registerBlockColors() {
        Minecraft.getMinecraft().getBlockColors().registerBlockColorHandler(INSTANCE, BreadBlocks.BREADWOOD_LEAVES);
    }
}
