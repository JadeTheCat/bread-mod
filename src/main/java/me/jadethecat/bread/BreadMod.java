package me.jadethecat.bread;

import me.jadethecat.bread.item.BreadItems;
import me.jadethecat.bread.item.BreadSmelt;
import me.jadethecat.bread.lib.Constants;
import me.jadethecat.bread.proxy.CommonProxy;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = Constants.MODID, name = Constants.MODNAME, version = Constants.VERSION, dependencies = Constants.DEPS)
public class BreadMod {
    @Mod.Instance
    public static BreadMod INSTANCE;

    public static Logger breadlog;

    @SidedProxy(clientSide = "me.jadethecat.bread.proxy.ClientProxy", serverSide = "me.jadethecat.bread.proxy.CommonProxy", modId = Constants.MODID)
    public static CommonProxy proxy;

    public static final CreativeTabs TAB_BREAD_MOD = (new CreativeTabs("tabBreadMod") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(BreadItems.BREAD_SLICE);
        }
    });

    @Mod.EventHandler
    public static void preInit(FMLPreInitializationEvent e) {
        breadlog = e.getModLog();
        proxy.preInit(e);
    }

    @Mod.EventHandler
    public static void init(FMLInitializationEvent e) {
        proxy.init(e);
        BreadSmelt.init();
    }

    @Mod.EventHandler
    public static void postInit(FMLPostInitializationEvent e) {
        proxy.postInit(e);
    }

    @Mod.EventHandler
    public static void serverStart(FMLServerStartingEvent e) {
        e.registerServerCommand(new CommandTPtoBread());
        e.registerServerCommand(new CommandTPtoOverworld());
    }
}
