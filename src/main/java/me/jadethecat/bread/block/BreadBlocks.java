package me.jadethecat.bread.block;

import me.jadethecat.bread.BreadMod;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod.EventBusSubscriber
@GameRegistry.ObjectHolder("bread")
public class BreadBlocks {

    public static Block BREADWOOD_LOG;
    public static Block BREADWOOD_LEAVES;
    public static Block BREADWOOD_SAPLING;
    public static Block BREADWOOD_PLANKS;
    public static Block BREADWOOD_PLANK_STAIRS;
    public static Block BREAD_DIM_TELEPORTER;
    public static Block SALT_BLOCK;
    public static Block BREAD_BLOCK;
    public static Block TOAST_BLOCK;
    public static Block BURNT_BREAD_BLOCK;
    public static Block DARK_SALT_BLOCK;
    public static Block BREADGRASS;
    public static Block BREADDIRT;
    public static Block BREADSTONE;
    public static Block BREADROCK;

    public static ItemBlock ITEM_BLOCK_BREADWOOD_LOG;
    public static ItemBlock ITEM_BLOCK_BREADWOOD_LEAVES;
    public static ItemBlock ITEM_BLOCK_BREADWOOD_SAPLING;
    public static ItemBlock ITEM_BLOCK_BREADWOOD_PLANKS;
    public static ItemBlock ITEM_BLOCK_BREADWOOD_PLANK_STAIRS;
    public static ItemBlock ITEM_BLOCK_BREAD_DIM_TELEPORTER;

    @SubscribeEvent
    public static void registerBlocks( RegistryEvent.Register<Block> e) {
        BREADWOOD_LOG = new BlockBreadwoodLog();
        BREADWOOD_LEAVES = new BlockBreadwoodLeaves();
        BREADWOOD_SAPLING = new BlockBreadwoodSapling();
        BREADWOOD_PLANKS = setBlockName(new BlockWood(), "breadwood_planks");
        BREADWOOD_PLANK_STAIRS = new BreadStairs("breadwood_plank_stairs", BREADWOOD_PLANKS.getDefaultState());
        BREAD_DIM_TELEPORTER = setBlockName(new BlockBreadTeleporter(), "dimension_teleporter");


        e.getRegistry().registerAll(BREADWOOD_LOG, BREADWOOD_LEAVES, BREADWOOD_SAPLING, BREADWOOD_PLANKS, BREADWOOD_PLANK_STAIRS, BREAD_DIM_TELEPORTER);
    }

    @SubscribeEvent
    public static void registerItemBlocks(RegistryEvent.Register<Item> e) {
        ITEM_BLOCK_BREADWOOD_LOG = setItemBlockName(new ItemBlock(BREADWOOD_LOG) {
            @Override
            public int getItemBurnTime(ItemStack itemStack) {
                return 300;
            }
        }, BREADWOOD_LOG.getRegistryName().getResourcePath());
        ITEM_BLOCK_BREADWOOD_LEAVES = setItemBlockName(new ItemBlock(BREADWOOD_LEAVES), BREADWOOD_LEAVES.getRegistryName().getResourcePath());
        ITEM_BLOCK_BREADWOOD_SAPLING = setItemBlockName(new ItemBlock(BREADWOOD_SAPLING) {
            @Override
            public int getItemBurnTime(ItemStack itemStack) {
                return 100;
            }
        }, BREADWOOD_SAPLING.getRegistryName().getResourcePath());
        ITEM_BLOCK_BREADWOOD_PLANKS = setItemBlockName(new ItemBlock(BREADWOOD_PLANKS), BREADWOOD_PLANKS.getRegistryName().getResourcePath());
        ITEM_BLOCK_BREADWOOD_PLANK_STAIRS = setItemBlockName(new ItemBlock(BREADWOOD_PLANK_STAIRS), BREADWOOD_PLANK_STAIRS.getRegistryName().getResourcePath());
        ITEM_BLOCK_BREAD_DIM_TELEPORTER = setItemBlockName(new ItemBlock(BREAD_DIM_TELEPORTER), BREAD_DIM_TELEPORTER.getRegistryName().getResourcePath());
        e.getRegistry().registerAll(ITEM_BLOCK_BREADWOOD_LOG, ITEM_BLOCK_BREADWOOD_LEAVES, ITEM_BLOCK_BREADWOOD_SAPLING, ITEM_BLOCK_BREADWOOD_PLANKS, ITEM_BLOCK_BREADWOOD_PLANK_STAIRS, ITEM_BLOCK_BREAD_DIM_TELEPORTER);
    }

    public static Block setBlockName(Block block, String name) {
        block.setRegistryName(name);
        block.setUnlocalizedName(name);
        return block;
    }

    public static ItemBlock setItemBlockName(ItemBlock itemBlock, String name) {
        itemBlock.setRegistryName(name);
        itemBlock.setUnlocalizedName(name);
        return itemBlock;
    }
}
