package me.jadethecat.bread.block;

import me.jadethecat.bread.BreadMod;
import net.minecraft.block.BlockStairs;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;

public class BreadStairs extends BlockStairs {
    protected BreadStairs(String name, IBlockState modelState) {
        super(modelState);
        setUnlocalizedName(name);
        setRegistryName(name);
        this.useNeighborBrightness = true;
        setCreativeTab(BreadMod.TAB_BREAD_MOD);
        setSoundType(SoundType.WOOD);
    }
}
