package me.jadethecat.bread.block;

import me.jadethecat.bread.world.BreadWorldGen;
import me.jadethecat.bread.world.dimension.TeleporterBread;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class BlockBreadTeleporter extends Block {
    public BlockBreadTeleporter() {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote) {
            if (playerIn.isSneaking()) {
                MinecraftServer server = worldIn.getMinecraftServer();
                if (worldIn.provider.getDimensionType().getId() != BreadWorldGen.BREAD_ID) {
                    playerIn.changeDimension(BreadWorldGen.BREAD_ID, new TeleporterBread(server.getWorld(BreadWorldGen.BREAD_ID), pos));
                } else {
                    playerIn.changeDimension(0, new TeleporterBread(server.getWorld(0), pos));
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
