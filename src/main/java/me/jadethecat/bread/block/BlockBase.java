package me.jadethecat.bread.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockBase extends Block {
    public BlockBase(Material materialIn, SoundType soundType, String name) {
        super(materialIn);
        setSoundType(soundType);
        setUnlocalizedName(name);
        setRegistryName(name);
    }
}
