package me.jadethecat.bread.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockWood extends Block {
    public BlockWood() {
        super(Material.WOOD);
        setSoundType(SoundType.WOOD);
    }
}
