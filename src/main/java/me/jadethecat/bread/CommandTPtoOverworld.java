package me.jadethecat.bread;

import net.minecraft.block.BlockPortal;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ITeleporter;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class CommandTPtoOverworld implements ICommand {
    @Override
    public String getName() {
        return "tpow";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "tpow";
    }

    @Override
    public List<String> getAliases() {
        List<String> a = new ArrayList<>();
        a.add("tpbread");
        return a;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayer) {
            EntityPlayer sender2 = (EntityPlayer) sender;
            sender2.changeDimension(0, (world, entity, yaw) -> {

            });
        }
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    @Override
    public int compareTo(ICommand o) {
        return 0;
    }
}
